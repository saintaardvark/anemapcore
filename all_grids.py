#!/usr/bin/env python

import click
import maidenhead as mh
import folium
import csv


qth_mh = 'CN89nf'
qth_latlon = mh.to_location(qth_mh)
default_zoom = 2


@click.group()
def all_grids():
    """Tool to generate map of all my QSOs
    """
    pass


def addMarker(m, grid, snr, call):
    """Add marker to a map.  Args:
    m: map to add to
    grid: maidenhead grid to use
    snr: snr reported by station
    call: callsign of statiion
    """
    try:
        folium.Marker(mh.to_location(grid),
                      popup=snr,
                      tooltip=call).add_to(m)
    except AssertionError:
        print("Skipping " + call + " because something wrong with grid")


def addQTH(m, grid, snr, call):
    """Add marker to a map for QTH.  Args:
    m: map to add to
    grid: maidenhead grid to use
    snr: snr reported by station
    call: callsign of statiion
    """
    try:
        folium.Marker(mh.to_location(grid),
                      popup=snr,
                      tooltip="QTH",
                      icon=folium.Icon(color='green')).add_to(m)
    except AssertionError:
        print("Skipping " + call + " because something wrong with grid")


def newMap():
    "Return a new map centered on qth"
    m = folium.Map(mh.to_location(qth_mh),
                   zoom_start=default_zoom)
    return m


def addCircle(m, centre):
    radius = 1000  # km; will be converted to metres later
    try:
        for i in range(1, 5):
            popup = "{} km".format(radius * i)
            diameter = radius * i * 1000  # metres
            folium.Circle(mh.to_location(centre),
                          radius=diameter,
                          popup=popup,
                          color='crimson',
                          fill=False).add_to(m)
    except AssertionError:
        print("Can't add circle, skipping")


@click.command('map',
               short_help='Generate map')
@click.option('--csv-file',
              '-c',
              default='all_grids.csv',
              show_default=True,
              help='CSV file with QSO info')
@click.option('--map-file',
              '-m',
              default='all_grids.html',
              show_default=True,
              help='Map file to write out')
@click.option('--qth',
              default='CN89nf',
              show_default=True,
              help='Add marker for QTH at this grid square')
def map(csv_file, map_file, qth):
    m = newMap()

    with open(csv_file, 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';')
        for row in spamreader:
            call = row[0]
            grid = row[1]
            date = row[2]
            if not grid:
                print("Skipping " + call + " because no grid")
                continue
            addMarker(m, grid, date, call)
            addQTH(m, qth, date, 'VA7UNX/VE3')
            addCircle(m, qth)
            m.save(map_file)


# Reminder: click doesn't automagically add the commands to the group
# (and thus to the help output); you have to do it manually.
all_grids.add_command(map)

if __name__ == '__main__':
    all_grids()
