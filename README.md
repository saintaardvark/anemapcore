# Anemapcore

A way to map Ham radio contacts.

# Generating a map with all_grids.py

`all_grids.py` is a Python3 script which generates an HTML file
containing an OpenStreetMap...er, map...of QSOs.  Currently it expects
to find, within the current directory, a file named `all_grids.csv`
that looks like this:

```
callsign;loc;qsodate;
N6RNP;CM99CR;2018-06-17;
XL75A;;2018-06-24;
K7VAN;CN84MW;2018-05-05;
W7XQ;CN85NU;2018-05-05;
WZ8T;CN85NU;2018-05-05;
K7FD;CN74XL;2018-06-28;
N7MZP;DN18RH;2018-06-30;
VE5RAC;;2018-07-01;
VE5FX;DO72XS;2018-07-01;
```

`loc` is the Maidenhead grid.  `qsodate` isn't parsed right now, so
the format isn't important.

The output, `all_grids.html`, is a map of all your contacts; a tool
tip for each will contain whatever's in the `qsodate` field.  Entries
with no Maidenhead grid (as, for example, VE5RAC up above) will be
skipped.

# Generating the CSV file from cqrlog

You can generate the CSV file from cqrlog by creating an SQL filter
like so:

```
select callsign,loc,qsodate from cqrlog_main
```

# Thanks

- [foswig](https://mrsharpoblunto.github.io/foswig.js/) for the name.
- [Leaflet.js](https://leafletjs.com/),
  [Folium](https://github.com/python-visualization/folium) and
  [OpenStreetMap](https://openstreetmap.org)...I'm constantly grateful
  that people have made such wonderful building blocks freely available.
