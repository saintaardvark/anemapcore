install:
	pip install -r requirements.txt
	@echo "FIXME: Maidenhead installation probably succeeded despite complaint about LICENSE file not being present.  See # https://github.com/scivision/maidenhead/issues/1"

dev:
	pip install -r requirements-dev.txt

jupyter:
	jupyter notebook

publish:
	source ./venv/bin/activate && ./all_grids.py map
	scp all_grids.html l2:~/public_html/random/

bigpoint:
	source ./venv/bin/activate && \
		./all_grids.py map \
			--csv-file ~/big_point_road.csv \
			--map-file big_point_road.html \
			--qth EN86bf
	scp big_point_road.html l2:~/public_html/random/big_point_road.html
